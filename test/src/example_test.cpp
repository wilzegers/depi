#include "depi/inject_function.hpp"
#include "depi/injector.hpp"
#include "depi/service_holder.hpp"

#include <iostream>

struct IA { virtual void doA() = 0; virtual ~IA() = default; };
struct IB { virtual void doB() = 0; virtual ~IB() = default; };
struct IC { virtual void doC() const = 0; virtual ~IC() = default; };

struct A : IA { void doA() override { std::cout << "A"; } };
struct B : IB { void doB() override { std::cout << "B"; } };
struct C : IC { void doC() const override { std::cout << "C"; } };

struct MyClass {
    MyClass(const IC*, IA&, std::shared_ptr<IB>) {}
};

using namespace depi;

int main() {
    A a;
    std::shared_ptr<IB> b;
    const IC* c = nullptr;

    MyClass x = detail::inject<MyClass>(a, b, c);

    auto inj = create_injector(
        bind<IA>::to(A{}),
        bind<IB>::to(b),
        bind<const IC>::to(std::make_unique<C>())
    );

    MyClass y = inj.template construct<MyClass>();

    (void)x;
    (void)y;
}