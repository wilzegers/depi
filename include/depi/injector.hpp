#pragma once

#include <utility>

#include "inject_function.hpp"

namespace depi
{

template <class... Services>
struct injector
{
    injector(Services &&... services)
        : tuple{std::forward<Services>(services)...}
    {
    }

    template <class T>
    T construct()
    {
        return detail::inject_tuple<T>(tuple);
    }

private:
    std::tuple<Services...> tuple;
};

template <class... Services>
auto create_injector(Services &&... services)
{
    return injector<
        std::remove_reference_t<Services>...>(std::forward<Services>(services)...);
}

} // namespace depi
