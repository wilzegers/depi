#pragma once

#include <type_traits>

#include "detail/type_list.hpp"
#include "detail/type_traits.hpp"

namespace depi {

namespace detail {

    
    template<
        class Service, // either lvalue or rvalue ref
        class CompatibilityList = type_list<std::remove_reference_t<Service>>
    >
    struct service_ref_holder {

        template<class T,
            class = std::enable_if_t<
                can_convert_list<
                    T&, map_t<std::add_lvalue_reference, CompatibilityList>
                >::value
            >
        >
        operator T&() {
            return service;
        }

        service_ref_holder(Service _service)
            : service{ std::forward<Service>(_service) }
        {}

    private:
        Service service;

    };

    template<
        class Service, // a raw type
        class CompatibilityList = type_list<Service>
    >
    struct service_raw_ptr_holder {

        template<class T,
            class = std::enable_if_t<
                can_convert_list<
                    T*, map_t<std::add_pointer, CompatibilityList>
                >::value
            >
        >
        operator T*() {
            return service;
        }

        service_raw_ptr_holder(Service* _service)
            : service{ std::move(_service) }
        {}

    private:
        Service* service;
    };

    template<class T>
    struct shared_decorator {
        using type = std::shared_ptr<T>;
    };

    template<
        class Service, // a raw type
        class CompatibilityList = type_list<std::shared_ptr<Service>>
    >
    struct service_shared_ptr_holder {

        template<class T,
            class = std::enable_if_t<
                can_convert_list<
                    std::shared_ptr<T>, CompatibilityList
                >::value
            >
        >
        operator std::shared_ptr<T>() {
            return service;
        }

        service_shared_ptr_holder(std::shared_ptr<Service> _service)
            : service{ std::move(_service) }
        {}

    private:
        std::shared_ptr<Service> service;
    };
    
    template<
        class Service, // a raw type
        class CompatibilityList = type_list<Service*>
    >
    struct service_unique_ptr_holder {

        template<class T>
        struct unique_decorator {
            using type = std::unique_ptr<T>;
        };

        template<class T,
            class = std::enable_if_t<
                can_convert_list<T*, CompatibilityList>::value
            >
        >
        operator T*() {
            return service.get();
        }

        service_unique_ptr_holder(std::unique_ptr<Service> _service)
            : service{ std::move(_service) }
        {}

    private:
        std::unique_ptr<Service> service;
    };

} // namespace detail

    template<class... Args>
    struct bind {
        template<
            class Service,
            class = std::enable_if_t<
                !detail::is_unique_ptr<Service>::value &&
                !detail::is_shared_ptr<Service>::value &&
                !std::is_pointer<Service>::value
            >
        >
        static auto to(Service&& service) {
            using interfaces = detail::map_t<std::add_lvalue_reference, detail::type_list<Args...>>;
            return detail::service_ref_holder<Service, interfaces>(std::forward<Service>(service));
        }

        template<class Service>
        static auto to(Service* service) {
            using interfaces = detail::map_t<std::add_pointer, detail::type_list<Args...>>;
            using holder_t = detail::service_raw_ptr_holder<Service, interfaces>;
            return holder_t(service);
        }

        template<class Service>
        static auto to(std::shared_ptr<Service> service) {
            using interfaces = detail::map_t<detail::shared_decorator, detail::type_list<Args...>>;
            using holder_t = detail::service_shared_ptr_holder<Service, interfaces>;
            return holder_t(std::move(service));
        }

        template<class Service>
        static auto to(std::unique_ptr<Service> service) {
            using interfaces = detail::map_t<std::add_pointer, detail::type_list<Args...>>;
            using holder_t = detail::service_unique_ptr_holder<Service, interfaces>;
            return holder_t(std::move(service));
        }
    };

} // namespace depi