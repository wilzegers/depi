#pragma once

#include <cstddef>

namespace depi {

namespace detail {

    template <bool Value = false>
    struct assert_not_type_list {
        constexpr assert_not_type_list() {
            static_assert(Value, "List parameter must be a \'depi::detail::type_list\'");
        }
    };

    template <bool Value = false>
    struct assert_empty_type_list {
        constexpr assert_empty_type_list() {
            static_assert(Value, "List parameter must not be an empty \'depi::detail::type_list\'");
        }
    };

    template<class... Args>
    struct type_list;

    template<class Tuple>
    struct list_from_tuple;

    template<class... Args>
    struct list_from_tuple<std::tuple<Args...>> {
        using type = type_list<Args...>;
    };

    template<class Tuple>
    using list_from_tuple_t = typename list_from_tuple<Tuple>::type;

    template<class New, class List>
    struct push_back {
        using type = decltype(assert_not_type_list<>());
    };

    template<class New, class... Contained>
    struct push_back<New, type_list<Contained...>> {
        using type = type_list<Contained..., New>;
    };

    template<class... Args>
    using push_back_t = typename push_back<Args...>::type;

    template<class New, class List>
    struct push_front {
        using type = decltype(assert_not_type_list<>());
    };

    template<class New, class... Contained>
    struct push_front<New, type_list<Contained...>> {
        using type = type_list<New, Contained...>;
    };

    template<class... Args>
    using push_front_t = typename push_front<Args...>::type;

    template<class List>
    struct head {
        using type = decltype(assert_not_type_list<>());
    };

    template<class Head, class... Tail>
    struct head<type_list<Head, Tail...>> {
        using type = Head;
    };

    template<>
    struct head<type_list<>> {
        using type = decltype(assert_empty_type_list<>());
    };

    template<class... Args>
    using head_t = typename head<Args...>::type;

    template<class List>
    struct tail {
        using type = decltype(assert_not_type_list<>());
    };

    template<class Head, class... Tail>
    struct tail<type_list<Head, Tail...>> {
        using type = type_list<Tail...>;
    };

    template<>
    struct tail<type_list<>> {
        using type = decltype(assert_empty_type_list<>());
    };

    template<class... Args>
    using tail_t = typename tail<Args...>::type;

    struct no_match;

    struct none;

    template<class T>
    struct meta_type {
        using type = T;
    };

    template<template<class> class Function, class List>
    struct find_if : std::conditional<
        Function<head_t<List>>::value,
        meta_type<head_t<List>>,
        find_if<Function, tail_t<List>>
    >::type {};

    template<template<class> class Function>
    struct find_if<Function, type_list<>> : meta_type<none> {};


    template<template<class> class Function, class List>
    using find_if_t = typename find_if<Function, List>::type;

    template<class List1, class List2>
    struct concat {
        using type = decltype(assert_not_type_list<>());
    };

    template<class... Contained1, class... Contained2>
    struct concat<type_list<Contained1...>, type_list<Contained2...>> {
        using type = type_list<Contained1..., Contained2...>;
    };

    template<class... Args>
    using concat_t = typename concat<Args...>::type;

    template<template<class...> class Fun, class List>
    struct apply {
        using type = decltype(assert_not_type_list<>());
    };

    template<template<class...> class Fun, class... Contained>
    struct apply<Fun, type_list<Contained...>> : Fun<Contained...> {};

    template<template<class...> class Fun, class List>
    using apply_t = typename apply<Fun, List>::type;

    template<template<class> class Fun, class List>
    struct map {
        using type = decltype(assert_not_type_list<>());
    };

    template<template<class> class Fun, class... Args>
    struct map<Fun, type_list<Args...>> {
        using type = type_list<typename Fun<Args>::type...>;
    };

    template<template<class> class Fun, class List>
    using map_t = typename map<Fun, List>::type;

    template<class Elem, int RepeatCount>
    struct repeat : push_back<Elem, typename repeat<Elem, RepeatCount - 1>::type> {
    };

    template<class Elem>
    struct repeat<Elem, 0> {
        using type = type_list<>;
    };

    template<class Elem, int RepeatCount>
    using repeat_t = typename repeat<Elem, RepeatCount>::type;

} // namespace detail

} // namespace depi