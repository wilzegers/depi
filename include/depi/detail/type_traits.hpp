#pragma once

#include <type_traits>

#include "type_list.hpp"

#ifndef DEPI_MAX_CONSTRUCTOR_PARAM_COUNT
#   define DEPI_MAX_CONSTRUCTOR_PARAM_COUNT 10
#endif

namespace depi {

namespace detail {

    template<class T>
    struct can_convert {
        template<class U>
        struct from : std::integral_constant<
            bool, std::is_convertible<U, T>::value
        > {};
    };

    template<class To, class List>
    struct can_convert_list {
        static constexpr bool value = !std::is_same<
            find_if_t<
                can_convert<To>::template from,
                List
            >,
            none
        >::value;
    };


    template<class List>
    struct any_of {
        template<class T,
            class = std::enable_if_t<
                can_convert_list<T, List>::value
                || can_convert_list<T&, List>::value
            >
        >
        operator T&() const;
    };

    constexpr int max_constructor_parameter = DEPI_MAX_CONSTRUCTOR_PARAM_COUNT;

    template<class T, class List, int Current = max_constructor_parameter>
    struct constructor_param_count : std::conditional_t<
        apply<
            std::is_constructible,
            push_front_t<
                T, typename repeat<any_of<List>, Current>::type
            >
        >::value,
        std::integral_constant<int, Current>,
        constructor_param_count<T, List, Current - 1>
    > {};

    template<class T, class List>
    struct constructor_param_count<T, List, 0> : std::integral_constant<int, -1> {};

    template<class T, class List, class Done, class Todo>
    struct constructor_params_impl
    {
        template<class U>
        using argument_guesser = apply_t<
            std::is_constructible,
            push_front_t<
                T,
                concat_t<
                    Done,
                    push_front_t<U, tail_t<Todo>>
                >
            >
        >;

        using choice = find_if_t<argument_guesser, List>;

        using type = typename constructor_params_impl<
            T,
            List,
            push_back_t<choice, Done>,
            tail_t<Todo>
        >::type;

    };

    template<class T, class List, class Done>
    struct constructor_params_impl<T, List, Done, type_list<>> {
        using type = Done;
    };

    template<class T, class List>
    struct constructor_params {
        static constexpr int param_count = constructor_param_count<T, List>::value;
        using type = typename constructor_params_impl<
            T,
            List,
            type_list<>,
            repeat_t<any_of<List>, param_count>
        >::type;
    };

    template<class... Args>
    using constructor_params_t = typename constructor_params<Args...>::type;

    template<template<class...> class Template, class T>
    struct is_instance_of : std::false_type {};

    template<template<class...> class Template, class T>
    struct is_instance_of<Template, const T> : is_instance_of<Template, T> {};

    template<template<class...> class Template, class T>
    struct is_instance_of<Template, volatile T> : is_instance_of<Template, T> {};

    template<template<class...> class Template, class T>
    struct is_instance_of<Template, T&> : is_instance_of<Template, T> {};

    template<template<class...> class Template, class T>
    struct is_instance_of<Template, T&&> : is_instance_of<Template, T> {};

    template<template<class...> class Template, class... Args>
    struct is_instance_of<Template, Template<Args...>> : std::true_type {};

    template<class T>
    using is_unique_ptr = is_instance_of<std::unique_ptr, T>;

    template<class T>
    using is_shared_ptr = is_instance_of<std::shared_ptr, T>;

    template<class T>
    using is_weak_ptr = is_instance_of<std::weak_ptr, T>;

} // namespace detail

} // namespace depi
