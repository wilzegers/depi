#pragma once

#include <utility>
#include <tuple>

namespace depi
{

namespace detail
{

template <std::size_t N, class T, class...Args>
struct get_impl_impl {};

template <std::size_t N, class T, class Arg, class...Args>
struct get_impl_impl<N, T, Arg, Args...> : get_impl_impl<N + 1, T, Args...> {};

template <std::size_t N, class T, class...Args>
struct get_impl_impl<N, T, T, Args...>
{
    template<typename Tuple>
    static auto get_from(Tuple&& tuple) -> decltype(std::get<N>(tuple))
    {
        return std::get<N>(tuple);
    }
};

template <class T, class Tuple>
struct get_impl {};

template <class T, class...Args>
struct get_impl<T, std::tuple<Args...>> : get_impl_impl<0, T, Args...>
{
};

template <class T, class Tuple>
auto get(Tuple&& tuple)
    -> decltype(
        get_impl<
            T,
            typename std::decay<Tuple>::type
        >::get_from(std::forward<Tuple>(tuple))
    )
{
    return get_impl<
        T,
        typename std::decay<Tuple>::type
    >::get_from(std::forward<Tuple>(tuple));
}

} // namespace detail

} // namespace depi
