#pragma once

#include <type_traits>
#include <tuple>
#include <memory>

#include "detail/type_traits.hpp"

namespace depi {

namespace detail {

    template<class T, class List>
    struct inject_impl;

    template<class T, class... Contained>
    struct inject_impl<T, type_list<Contained...>> {

        template<class Tuple>
        static T create(Tuple& tuple) {
            return T{ std::get<Contained>(tuple)... };
        }
    };

    template<class T, class Tuple>
    T inject_tuple(Tuple& args) {
        using constructor_args = constructor_params_t<T, list_from_tuple_t<Tuple>>;
        return inject_impl<T, constructor_args>::create(args);
    }

    template<class T, class... Args>
    T inject(Args&... args) {
        std::tuple<Args&...> arg_tuple{ args... };
        return inject_tuple<T>(arg_tuple);
    }

} // namespace detail

} // namespace depi