# depi

A minimal dependency injection implementation using the C++14 standard.

Example usage:

```cpp

struct IA { virtual void doA() = 0; };
struct IB { virtual void doB() = 0; };
struct IC { virtual void doC() const = 0; };

struct A : IA { void doA() override; };
struct B : IB { void doB() override; };
struct C : IC { void doC() const override; };

struct MyClass {
    MyClass(const IC*, IA&, std::shared_ptr<IB>) {}
};

int main()
{
    using namespace depi;

    std::shared_ptr<IB> b = std::make_shared<B>();

    auto inj = create_injector(
        bind<IA>::to(A{}),
        bind<IB>::to(b),
        bind<const IC>::to(std::make_unique<C>())
    );

    MyClass y = inj.template construct<MyClass>();
}

```